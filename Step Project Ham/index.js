
document.querySelector(".magnifying-glass").onclick = () => {
    const showInput = document.getElementById('input');
    return showInput.style.display = 'block'
};

const elementsArray = [...document.querySelectorAll(".tabs-title")];
const contentArray = [...document.querySelectorAll(".tabs-content li")];

document.querySelector(".tabs").onclick = (event) => {
    removeActive()
    event.target.classList.add("active");
    document.querySelector(`.tabs-content li[data-tab="${event.target.dataset.tab}"]`).classList.add("active");
};

function removeActive() {
    const clsActive = [...document.querySelectorAll('.active')];
    return clsActive.map((el) => {
        return(el.classList.remove('active'))
    });
}

elementsArray.forEach((el, i) => {
    el.dataset.tab = el.textContent;
    contentArray[i].dataset.tab = el.textContent;
    elementsArray[i].dataset.tab = elementsArray[i].textContent;
});